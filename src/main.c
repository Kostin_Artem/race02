#include "way_home.h"
#include "map.h"
#include "validator.h"
#include "parser.h"
#include "path.h"

int main(int argc, char **argv) {
    t_map *map = mx_create_map();
    t_vstatus status;
    t_list **results;
    if (argc != 6) {
        mx_printerr(VS_USAGE);
        exit(EXIT_SUCCESS);
    } else if (map == NULL) {
        mx_printerr(VS_UNKNOWN);
        exit(EXIT_SUCCESS);
    } else if((status = mx_validate_map(argv[1], map)) != VS_VALID) {
        mx_free_map(&map);
        mx_printerr(status);
        exit(EXIT_SUCCESS);
    } else if((status = mx_load_map(argv[1], map)) != VS_VALID) {
        mx_free_map(&map);
        mx_printerr(status);
        exit(EXIT_SUCCESS);
    } else if((status = mx_validate_args(map, argv)) != VS_VALID) {
        mx_free_map(&map);
        mx_printerr(status);
        exit(EXIT_SUCCESS);
    }
    results = mx_find_path(map);
    if (results == NULL) {
        mx_free_map(&map);
        mx_printerr(VS_UNKNOWN);
        exit(EXIT_SUCCESS);
    }
    if (results[0] == NULL) {
        mx_free_map(&map);
        mx_clear_list(results + 2);
        free(results);
        mx_printerr(VS_ROUTE);
        exit(EXIT_SUCCESS);
    }
    mx_draw_path(map, results[0]);
    mx_show_worst(map, results[1]);
    
    mx_print(1, "dist=");
    mx_printint(results[1]->cost);
    write(STDOUT_FILENO, "\n", 1);
    mx_print(1, "exit=");
    mx_printint(results[0]->cost);
    write(STDOUT_FILENO, "\n", 1);

    if((status = mx_write_map_file("path.txt", map)) != VS_VALID)
        mx_printerr(status);  // No need to clean before return.
    mx_clear_list(results + 2);
    free(results);
    mx_free_map(&map);
    exit(EXIT_SUCCESS);
}
