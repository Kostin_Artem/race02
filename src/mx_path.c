#include "path.h"

t_list **mx_find_path(t_map *map) {
    t_list *explored = NULL;
    t_list *reachable = mx_create_node(&map->entry, NULL, 0);
    t_list *res = NULL;

    while (reachable != NULL) {
        t_list *node = mx_choose_node(&reachable, &map->exit);
        if (res == NULL && mx_eq_point(&node->data, &map->exit))
            res = node;
        mx_push_back(&explored, node);
        t_list *neighbors = mx_get_neighbors(node, map);

        for (t_list *cur = neighbors; cur != NULL;) {
            if (!mx_in_list(explored, &cur->data, mx_eq_point)) {
                if (!mx_in_list(reachable, &cur->data, mx_eq_point)) {
                    t_list *tmp = cur->next;
                    mx_push_back(&reachable, cur);
                    cur = tmp;
                    continue;
                } else {
                    t_list *tmp = mx_find_in_list(reachable, &cur->data, mx_eq_point);
                    if (node->cost + 1 < tmp->cost) {
                        tmp->from = node;
                        tmp->cost = node->cost + 1;
                    }
                }
            } else {
                t_list *tmp = mx_find_in_list(explored, &cur->data, mx_eq_point);
                if (node->cost + 1 < tmp->cost) {
                    tmp->from = node;
                    tmp->cost = node->cost + 1;
                }
            }
            t_list *tmp = cur;
            cur = cur->next;
            mx_free_node(&tmp);
        }
    }
    {
        t_list *worst = NULL;
        t_list **results = malloc(sizeof(t_list **) * 3);
        results[0] = res;

        int max_cost = mx_max_cost(explored);
        for (t_list *cur = explored; cur != NULL;) {
            if (cur->cost == max_cost) {
                t_list *tmp = cur->next;
                mx_push_back(&worst, cur);
                cur = tmp;
            } else {
                cur = cur->next;
            }
        }
        results[1] = worst;
        results[2] = explored;

        return results;
    }
}

void mx_draw_path(t_map *map, t_list *path) {
    const t_point *p = NULL;
    for (t_list *cur = path; cur != NULL; cur = cur->from) {
        p = &cur->data;
        map->map[p->y][p->x] = '*';
    }
}

void mx_show_worst(t_map *map, t_list *list) {
    const t_point *p = NULL;
    for (t_list *cur = list; cur != NULL; cur = cur->next) {
        p = &cur->data;
        map->map[p->y][p->x] = map->map[p->y][p->x] == '*' ? 'X' : 'D';
    }
}
