#include "validator.h"

static t_vstatus validate_fformat(int fd, t_map *map) {
    int status;
    unsigned int curr_width = 0;
    char buf;
    bool is_element = false;
    while ((status = read(fd, &buf, 1)) > 0) {
        if (buf == '\n') {
            if (!is_element)  // Must be only one control element in a row.
                return VS_MAP_ERR;
            is_element = false;
            ++map->height;
            if (map->width == 0)  // Only for the first time.
                map->width = curr_width;
            if (map->width != curr_width)  // Map must be rectangle.
                return VS_MAP_ERR;
            curr_width = 0;
        } else if (buf == ',') {
            if (!is_element)  // Must be only one control element in a row.
                return VS_MAP_ERR;
            is_element = false;
        } else if (mx_is_valid_symbol(buf)) {
            if (is_element)  // Must be only one map element in a row.
                return VS_MAP_ERR;
            is_element = true;
            ++curr_width;
        } else
            return VS_MAP_ERR;  // Incorrect symbol.
    }
    if (status == -1)
        return VS_UNKNOWN;
    if (map->width == 0)
        return VS_NO_FILE;
    if (map->height == 0 || buf != '\n')
        return VS_MAP_ERR;
    return VS_VALID;
}

t_vstatus mx_validate_map(const char *filename, t_map *map) {
    int fd;
    t_vstatus status;
    if (filename == NULL || map == NULL)
        return VS_UNKNOWN;
    fd = open(filename, O_RDONLY);
    if (fd == -1)
        return VS_NO_FILE;
    status = validate_fformat(fd, map);
    close(fd);
    return status;
}

static t_vstatus validate_point(t_map *map, t_point *p, char *x, char *y) {
    long long number;
    if (mx_atoi(x, &number) != VS_VALID)
        return VS_UNKNOWN;
    if (number < 0 || number >= map->width)
        return VS_OUT_OF_RANGE;
    p->x = (unsigned int)number;

    if (mx_atoi(y, &number) != VS_VALID)
        return VS_UNKNOWN;
    if (number < 0 || number >= map->height)
        return VS_OUT_OF_RANGE;
    p->y = (unsigned int)number;

    return VS_VALID;
}

t_vstatus mx_validate_args(t_map *map, char **argv) {
    t_vstatus status = validate_point(map, &map->entry, argv[2], argv[3]);
    if (status != VS_VALID)
        return status;
    if (map->map[map->entry.y][map->entry.x] != '.')
        return VS_ENTRY_OBSTACLE;
    status = validate_point(map, &map->exit, argv[4], argv[5]);
    if (status != VS_VALID)
        return status;
    if (map->map[map->exit.y][map->exit.x] != '.')
        return VS_EXIT_OBSTACLE;
    return VS_VALID;
}
