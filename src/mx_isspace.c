#include <stdbool.h>

bool mx_isspace(char c) {
    /*
     * See "man isspace".
     */
    if (c == ' '
        || c == '\t'
        || c == '\n'
        || c == '\v'
        || c == '\f'
        || c == '\r') {
        return true;
    }

    return false;
}
