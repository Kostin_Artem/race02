.OBJECTS = $(wildcard src/*.c)
OBJECTS = $(.OBJECTS:src%.c=obj%.o)

all: prepare way_home

prepare:
	mkdir -p obj/

way_home: $(OBJECTS)
	clang -g -std=c11 -Wall -Wextra -Werror -Wpedantic -Iinc -o $@ $^

obj/%.o: src/%.c
	clang -g -std=c11 -Wall -Wextra -Werror -Wpedantic -Iinc -o $@ -c $<

clean:
	rm -rf inc
	rm -rf src
	rm -rf obj

uninstall:
	rm -rf obj
	rm -f way_home

reinstall: uninstall all
