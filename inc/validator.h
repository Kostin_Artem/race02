#pragma once
#include "way_home.h"
#include "map.h"

t_vstatus mx_validate_map(const char *filename, t_map *map);
t_vstatus mx_validate_args(t_map *map, char **argv);
