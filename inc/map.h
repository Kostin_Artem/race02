#pragma once

#include "way_home.h"

typedef struct s_point {
    int x;
    int y;
}              t_point;

typedef struct s_map {
    char **map;
    unsigned int width;
    unsigned int height;
    t_point entry;
    t_point exit;
}              t_map;

void mx_write_map(int fd, t_map *map);
t_vstatus mx_write_map_file(const char *filename, t_map *map);

// Allocates memory for the t_map and initializes fields with 0.
t_map *mx_create_map();

void mx_free_map(t_map **map);

// '.' - space, '#' - obstacle.
bool mx_is_valid_symbol(char c);
