#pragma once

#include "list.h"
#include "point.h"
#include "map.h"

t_list **mx_find_path(t_map *map);

void mx_draw_path(t_map *map, t_list *path);

void mx_show_worst(t_map *map, t_list * list);
